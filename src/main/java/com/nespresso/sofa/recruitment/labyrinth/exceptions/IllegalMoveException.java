package com.nespresso.sofa.recruitment.labyrinth.exceptions;

public class IllegalMoveException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	
	public IllegalMoveException() {
		super("Its an illegal move as there is no door to destination room");
	}

}
