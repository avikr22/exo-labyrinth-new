package com.nespresso.sofa.recruitment.labyrinth.exceptions;

public class ClosedDoorException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public ClosedDoorException(){
		super("Door is closed");
	}
}
