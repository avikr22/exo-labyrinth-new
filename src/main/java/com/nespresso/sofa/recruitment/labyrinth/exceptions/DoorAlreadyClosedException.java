package com.nespresso.sofa.recruitment.labyrinth.exceptions;

public class DoorAlreadyClosedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DoorAlreadyClosedException(){
		super("Door is Already closed");
	}

}
