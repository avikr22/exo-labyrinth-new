package com.nespresso.sofa.recruitment.labyrinth;

public enum DoorType {
	GATE('|'), GATE_SENSOR('$');

	private char gate;

	private DoorType(char gate) {
		this.gate = gate;
	}

	public static DoorType getDoorType(char gate) {
		for (DoorType door : values()) {
			if (door.gate == gate) {
				return door;
			}
		}
		return null;
	}
}
