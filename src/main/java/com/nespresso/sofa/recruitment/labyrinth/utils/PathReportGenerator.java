package com.nespresso.sofa.recruitment.labyrinth.utils;

public class PathReportGenerator {
	private StringBuilder report;
	private final String ROOM_SEPARATOR = ";";

	public PathReportGenerator() {
		report = new StringBuilder();
	}

	public void updateReport(final String currentRoom, final String previousRoom) {
		report.append(previousRoom).append(currentRoom).append(ROOM_SEPARATOR);
	}

	public String generateReport() {
		StringBuilder finalReport = new StringBuilder(report);
		int reportLenth = finalReport.length();
		finalReport.deleteCharAt(reportLenth - 1);
		return finalReport.toString();
	}

}
