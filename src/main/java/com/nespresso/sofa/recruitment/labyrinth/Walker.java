package com.nespresso.sofa.recruitment.labyrinth;

import java.util.Map;

import com.nespresso.sofa.recruitment.labyrinth.exceptions.ClosedDoorException;
import com.nespresso.sofa.recruitment.labyrinth.exceptions.IllegalMoveException;
import com.nespresso.sofa.recruitment.labyrinth.model.Room;
import com.nespresso.sofa.recruitment.labyrinth.utils.PathReportGenerator;

public class Walker {
	private Map<String, Room> roomMap;
	private String previousRoom;
	private String currentRoom;
	private PathReportGenerator report;

	public Walker(Map<String, Room> roomMap) {
		this.roomMap = roomMap;
		report = new PathReportGenerator();
	}

	public void walkTo(String destination) throws IllegalMoveException,
			ClosedDoorException {
		Room current = roomMap.get(currentRoom);
		if (roomMap.containsKey(destination)
				&& current.isWalkAllowed(destination)) {

			previousRoom = currentRoom;
			currentRoom = destination;

			updateTheSensorReport(current, destination);

		} else {
			throw new IllegalMoveException();
		}

	}

	private void updateTheSensorReport(Room current, String destination) {
		if (current.isTheDoorHavingSensor(destination)) {
			report.updateReport(currentRoom, previousRoom);
		}

	}

	public String position() {

		return currentRoom;
	}

	public void closeLastDoor() {
		System.out.println("C ::" + currentRoom);
		System.out.println("P::: " + previousRoom);
		Room current = roomMap.get(currentRoom);
		Room previous = roomMap.get(previousRoom);

		current.closeTheDoor(previousRoom);
		System.out.println("P11::: " + previousRoom);
		previous.closeTheDoor(currentRoom);

	}

	public void popIn(String room) {
		if (roomMap.containsKey(room)) {
			this.currentRoom = room;
		} else {
			throw new IllegalMoveException();
		}

	}

	public String readSensors() {
		return report.generateReport();
	}

}
