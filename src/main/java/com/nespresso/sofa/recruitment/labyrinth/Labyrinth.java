package com.nespresso.sofa.recruitment.labyrinth;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.sofa.recruitment.labyrinth.model.Room;
import com.nespresso.sofa.recruitment.labyrinth.utils.LabyrinthParser;

public class Labyrinth {
	private LabyrinthParser parser;
	private Map<String, Room> roomMap; // Need to create only one object for a given room
	private Walker walker;
	
	public Labyrinth(String... pathDetails) {
		roomMap = new HashMap<String, Room>();
		parser = new LabyrinthParser();
		
		parser.initializePath(pathDetails, roomMap);
	}

	public Walker popIn(String room) {
		walker = new Walker(roomMap);
		walker.popIn(room);
		return walker;
	}

	public String readSensors() {
		return walker.readSensors();
	}

}
