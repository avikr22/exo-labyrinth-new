package com.nespresso.sofa.recruitment.labyrinth.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nespresso.sofa.recruitment.labyrinth.DoorType;
import com.nespresso.sofa.recruitment.labyrinth.exceptions.ClosedDoorException;
import com.nespresso.sofa.recruitment.labyrinth.exceptions.DoorAlreadyClosedException;
import com.nespresso.sofa.recruitment.labyrinth.exceptions.IllegalMoveException;

public class Room {

	// Here the Key will be the attached room the current room and value the
	// door type
	private Map<String, DoorType> attachedRooms;
	private List<String> attachedRoomsWithClosedGates;

	public Room() {
		attachedRooms = new HashMap<String, DoorType>();
		attachedRoomsWithClosedGates = new ArrayList<String>();
	}

	public void updateAttachedRooms(final DoorType door, final String destination) {
		attachedRooms.put(destination, door);
	}
	
	public boolean isWalkAllowed(final String destinationRoom) {
		if(attachedRooms.containsKey(destinationRoom)) {
			return true;
		} else if(attachedRoomsWithClosedGates.contains(destinationRoom)){
			throw new ClosedDoorException();
		} else {
			throw new IllegalMoveException();
		}
	}

	public void closeTheDoor(String previousRoom) {
		if(attachedRooms.containsKey(previousRoom)) {
			attachedRooms.remove(previousRoom);
			attachedRoomsWithClosedGates.add(previousRoom);
		} else {
			throw new DoorAlreadyClosedException();
		}
		
	}
	
	public boolean isTheDoorHavingSensor(String destination) {
		DoorType door = attachedRooms.get(destination);
		if(door == DoorType.GATE_SENSOR) {
			return true;
		}
		return false;
	}

}
