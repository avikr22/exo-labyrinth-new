package com.nespresso.sofa.recruitment.labyrinth.utils;

import java.util.Map;

import com.nespresso.sofa.recruitment.labyrinth.DoorType;
import com.nespresso.sofa.recruitment.labyrinth.model.Room;

public class LabyrinthParser {

	public void initializePath(String[] pathDetails, Map<String, Room> roomMap) {
		for (String path : pathDetails) {
			char[] gateDetail = path.toCharArray();
			String sourceRoom = Character.toString(gateDetail[0]);
			DoorType door = DoorType.getDoorType(gateDetail[1]);
			String destinationRoom = Character.toString(gateDetail[2]);
			if(!roomMap.containsKey(sourceRoom)) {
				roomMap.put(sourceRoom, new Room());
			}
			if(!roomMap.containsKey(destinationRoom)) {
				roomMap.put(destinationRoom, new Room());
			}
			
			//Attach the rooms which are adjacent
			attachRooms(sourceRoom,door,destinationRoom,roomMap);
		}

	}

	private void attachRooms(String source, DoorType door,
			String destination, Map<String, Room> roomMap) {
		
		Room sourceRoom = roomMap.get(source);
		sourceRoom.updateAttachedRooms(door, destination);
		
		Room destinationRoom = roomMap.get(destination);
		destinationRoom.updateAttachedRooms(door, source);
		
	}

}
